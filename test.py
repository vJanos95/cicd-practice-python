import sys

input_file = "input.txt"
output_file = "output.txt"

with open(input_file,"r") as in_file, open(output_file,"r") as out_file:
    for row in zip(in_file.readlines(),out_file.readlines()):
        if int(row[0]) == int(row[1])/2:
            continue
        else:
            sys.exit(1)